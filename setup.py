from setuptools import setup, find_packages

# read in version string
__version__ = "0.0.2" 

# README
with open("README.md", "r") as fh:
    long_description = fh.read()

# requires
reqs = ['torch',
        'numpy',
        'matplotlib',
        'cvxopt',
        'tqdm',
        'pykeops'
        ]

# SETUP
setup(
    name='kernelquantifier',
    version=__version__,
    packages=find_packages(),
    description='Quantification using Kernel Embedding and Random Fourier Features. On GPU and CPU.',
    url='https://gitlab.com/bastiendussapapb/kernelquantifier',
    install_requires=reqs,
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Development Status :: 4 - Beta",
        "Environment :: GPU :: NVIDIA CUDA",
        "License :: OSI Approved :: MIT License",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Mathematics",
    ],
    author='Bastien Dussap',
    author_email='bastien.dussap@inria.fr',
    license='MIT',
    long_description=long_description,
    long_description_content_type="text/markdown",
    project_urls={
        'KernelQuantification pypi': 'https://gitlab.com/bastiendussapapb/kernelquantifier',
    }
)