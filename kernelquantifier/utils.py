# -*- coding: utf-8 -*-

"""
@author: Dussap Bastien
"""

import torch
import numpy as np
from torch.cuda import is_available, memory_allocated, memory_reserved, get_device_name


def cuda_info(dev):
    """Gives information about CUDA"""

    # setting device on GPU if available, else CPU
    print('Using device:', dev)
    print()

    # Additional Info when using cuda
    if dev.type == 'cuda':
        print(get_device_name(0))
        print('Memory Usage:')
        print('Allocated:', round(memory_allocated(0)/1024**3, 1), 'GB')
        print('Cached:   ', round(memory_reserved(0)/1024**3, 1), 'GB')

    print(torch.version.cuda)

def angle_gram_matrix(b:list[torch.tensor])->torch.tensor:
    """
    Compute the "angle gram matrix M
    M_ij = <bi - mean(b), bj - mean(b)>

    Args:
        b (list[torch.tensor]): mu(P_i)

    Returns:
        torch.tensor: M
    """
    c = len(b)
    M = torch.zeros((c,c))
    bm = torch.mean(torch.cat(b), axis=0)
    for i in range(c):
        for j in range(i, c):
            M[i,j] = (b[i] - bm) @ (b[j] - bm)
            M[j,i] = M[i,j] 
    
    return M

def choose_device(verbose=False):
    """ If cuda is avalaible returns pytorch device("cuda:0"). device("cpu") otherwise """
    if is_available():
        dev = torch.device("cuda:0")
        if verbose:
            print("Running on the GPU")
    else:
        dev = torch.device("cpu")
        if verbose:
            print("Running on the CPU")

    return(dev)

def subsampling(P, size_sampling):
    """
    Parameters
    ----------
    P : torch.tensor
    size_sampling : int
    Returns
    -------
    torch.tensor
    """

    size = int(min(P.shape[0], size_sampling))
    gen = np.random.default_rng()
    return P[gen.choice(P.shape[0], size, replace=False)]


def subsampling_list(P, size_sampling):
    """
    Same as subsampling but for a list.
    Parameters
    ----------
    P : list of torch.tensor
    size_sampling : int
    Returns
    -------
    list of torch.tensor
    """
    return [subsampling(p, size_sampling) for p in P]
