# -*- coding: utf-8 -*-

"""
This .py contains metrics.
See available_metrics() for all the metrics.

@author: Dussap Bastien
"""

import numpy as np
from numpy.linalg import norm

def measure_performance(pi, pi_hat):
    """
    
    """
    dict = {}
    for k in available_metrics():
        dict[k] = select_metrics(k)(pi, pi_hat)
    return(dict)


def KL_divergence(pi, pi_hat):
    """
    KL Divergence
    """

    pi = np.array(pi)
    pi_hat = np.array(pi_hat)

    return np.sum(np.where(pi != 0, pi * np.log(pi/ pi_hat), 0))

def normalised_KL_divergence(pi, pi_hat):
    """
    Normalised KL divergence
    """

    def sigmoid(x):
        return 1/(1 + np.exp(-x))

    kl = KL_divergence(pi, pi_hat)
    return 2*sigmoid(kl)-1

def absolute_error(pi, pi_hat):
    """
    Absolute error
    """

    pi = np.array(pi)
    pi_hat = np.array(pi_hat)
    return 100*np.mean(np.abs(pi_hat - pi))

def relative_error(pi, pi_hat):
    """
    Relative Absolute Error
    """

    pi = np.array(pi)
    pi_hat = np.array(pi_hat)

    return 100*np.mean(np.abs(pi_hat - pi)/pi)

def hellinger(pi, pi_hat):
    "Hellinger distance"
    return norm(np.sqrt(pi) - np.sqrt(pi_hat)) / np.sqrt(2)

__metrics__ = {
    "KL" : KL_divergence,
    "NKL" : normalised_KL_divergence,
    "AE" : absolute_error,
    "RE" : relative_error,
    "H" : hellinger
    }

def available_metrics()->list[str]:
    "Return the available metrics"
    return list(__metrics__.keys())

def select_metrics(select_metrics:str):
    "Return the function associated to a given metric."
    assert select_metrics in available_metrics(), f"{select_metrics} not implemented, current metrics : {available_metrics()}"
    return __metrics__[select_metrics]