# -*- coding: utf-8 -*-

"""
@author: Dussap Bastien
This .py contains the function to plot the results of our quantifiers.
"""

from .data import LabelledCollection
from .kquant import KernelQuantifier, KernelQuantifierRFF
from .gkquant import GenerativeKernelQuantifier

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def bar_plot(data: LabelledCollection, target_prop:np.array, *quantifier):
    """ Visualise the agreement between the quantifiers and the true proportions.

    Args:
        data (LabelledCollection): Source data
        target_prop (np.array): True proportions
        *quantifer: The quantifiers.
    """

    df_dict = {"Population": list(data.name_classes),
               "Proportion": target_prop.tolist(),
               "Method": ["True proportions"]*len(data),
               }
    
    for q in quantifier:
        for (i, prop) in enumerate(q.props_):
            df_dict["Population"].append(data.name_classes[i])
            df_dict["Proportion"].append(prop)

            if type(q) == KernelQuantifier:
                df_dict["Method"].append("KernelQuantifier")
            elif type(q) == KernelQuantifierRFF:
                df_dict["Method"].append("KernelQuantifierRFF")
            elif type(q) == GenerativeKernelQuantifier:
                df_dict["Method"].append("GenerativeKernelQuantifier") 
            else:
                df_dict["Method"].append("Unspecified method") 

    plt.figure(figsize=(12, 7))
    sns.set_theme()
    sns.barplot(x='Population', y='Proportion', hue='Method', data=pd.DataFrame(df_dict))
    plt.legend(loc='best')
    plt.xlabel('Population', size=14)
    plt.ylabel('Proportion', size=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    #plt.title(title, fontweight="bold", loc='left', size=16)
    plt.show()

def KL_plot(generativequantifier:GenerativeKernelQuantifier, target_prop:np.array):
    """Visualise the KL divergence between the esimate and the true proportions trought the iterations.

    Args:
        generativequantifier (GenerativeKernelQuantifier): The quantifier.
        target_prop (np.array): True proportions.
    """
    kl = generativequantifier.get_kl(target_prop=target_prop)
    df_dict = {
        "index": list(np.arange(len(kl))),
        "value": kl,
    }

    plt.figure(figsize=(12, 7))
    sns.lineplot(data=pd.DataFrame(df_dict), x="index", y="value")
    #plt.legend(loc='best')
    plt.xlabel('Iteration', size=20)
    plt.ylabel(r'KL$(\hat{p}|p)$', size=20)
    plt.xticks(size=18)
    plt.yticks(size=18)
    #plt.title(title, fontweight="bold", loc='left', size=16)
    plt.show()